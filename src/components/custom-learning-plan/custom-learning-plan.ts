import { Component, OnInit } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { CoreSitesProvider } from '@providers/sites';
import { SpotonCustomProvider } from '@providers/spoton-custom/spoton-custom';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the CustomLearningPlanComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-learning-plan',
  templateUrl: 'custom-learning-plan.html',
  styleUrls: ['/custom-learning-plan.css']
})
export class CustomLearningPlanComponent implements OnInit {
  learningPlan: Observable<any>;
  myLearningPlan = [];
  userId:number;
  siteurl: string;
  token: string;
  baseUrl: string;

  constructor(public httpClient: HttpClient, public SpotonCustomProvider : SpotonCustomProvider,  public sitesProvider: CoreSitesProvider, private navCtrl: NavController) {
    this.learningPlan = this.SpotonCustomProvider.fetchLearningPlan( this.SpotonCustomProvider.learningPlan );
    this.learningPlan
    .subscribe(data => {
      console.log('my data: ', data);
      this.myLearningPlan = data.data;
    });
  }

  ngOnInit(): void { 
  }

  mylearningPlanRedirect(plans) {
    this.navCtrl.push('MyLearningPlanPage', { plans: plans });
  }

}
