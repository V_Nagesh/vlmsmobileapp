import { Component, Input } from '@angular/core';

/**
 * Generated class for the CustomProgressBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-progress-bar',
  templateUrl: 'custom-progress-bar.html',
  styleUrls: ['/custom-progress-bar.css']
})
export class CustomProgressBarComponent {
  @Input('progress') progress;

  constructor() {
    console.log('Hello CustomProgressBarComponent Component');
  }

}
