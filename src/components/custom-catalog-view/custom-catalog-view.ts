import { Component } from '@angular/core';
import { AlertController, ToastController  } from 'ionic-angular';
import { CoreSitesProvider } from '@providers/sites';
import { SpotonCustomProvider } from '@providers/spoton-custom/spoton-custom';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { CoreDomUtilsProvider } from '@providers/utils/dom';

/**
 * Generated class for the CustomCatalogViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-catalog-view',
  templateUrl: 'custom-catalog-view.html',
  styleUrls: ['/custom-catalog-view.css']
})
export class CustomCatalogViewComponent {
  catalog: Observable<any>;
  catalogOverView = [];
  userId:number;
  siteurl: string;
  token: string;
  baseUrl: string;
  errorMsg: string;
  errorFlag = false;

  constructor(public httpClient: HttpClient,  private domUtils: CoreDomUtilsProvider, public sitesProvider: CoreSitesProvider, public SpotonCustomProvider : SpotonCustomProvider, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    this.catalogData();
  }

  catalogData() {
    this.catalog = this.SpotonCustomProvider.fetchCatalog( this.SpotonCustomProvider.catalog );
    this.catalog
    .subscribe((data: any) => {
      console.log('my data: ', data);
      this.catalogOverView = data.data;
    }, (err) =>{
        this.errorFlag = true;
        this.errorMsg = err.message;
        const toast = this.toastCtrl.create({
            message: err.message,
            duration: 2000,
            position: "bottom"
        });
        toast.present(); 
    });
  }

  enrollUser(e?: Event, courseId?: any, enrollId?: any): void {
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    const confirm = this.alertCtrl.create({
      title: 'Please Confirm',
      message: 'Click confirm to enroll yourself.',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            const modal = this.domUtils.showModalLoading();

            let enrollStatus = this.SpotonCustomProvider.fetchEnrollStatus( this.SpotonCustomProvider.enrolUrl, courseId, enrollId );
            enrollStatus.subscribe( (response: any) => {
              modal.dismiss();

              if( response.data.status === true){
                const toast = this.toastCtrl.create({
                  message: 'You are enrolled successfully',
                  duration: 2000,
                  position: "bottom"
                });
                toast.present(); 

                this.catalogData();
              } else {
                const toast = this.toastCtrl.create({
                  message: "Enrolment is disabled or inactive",
                  duration: 2000,
                  position: "bottom"
                });
                toast.present(); 
              }
            }); 
          }
        }
      ]
    });
    confirm.present();
  }

  unEnrollUser(e?: Event, courseId?: any, enrollId?: any): void {
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    const confirm = this.alertCtrl.create({
      title: 'Please Confirm',
      message: 'Are you sure , you want to unenrol yourself?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            const modal = this.domUtils.showModalLoading();

            let enrollStatus = this.SpotonCustomProvider.fetchUnenrollStatus( this.SpotonCustomProvider.unenrolUrl, courseId, enrollId );
            enrollStatus.subscribe( (response: any) => {
              modal.dismiss();
              
              if( response.data.status === true){
                const toast = this.toastCtrl.create({
                  message: 'You are unenrolled successfully',
                  duration: 2000,
                  position: "bottom"
                });
                toast.present(); 
                this.catalogData();
              } else {
                const toast = this.toastCtrl.create({
                  message: "Enrolment is disabled or inactive",
                  duration: 2000,
                  position: "bottom"
                });
                toast.present(); 
              }
            });
            
          }
        }
      ]
    });
    confirm.present();
  }

}
