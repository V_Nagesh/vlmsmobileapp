import { Component, OnInit } from '@angular/core';
import { ToastController  } from 'ionic-angular';
import { CoreSitesProvider } from '@providers/sites';
import { SpotonCustomProvider } from '@providers/spoton-custom/spoton-custom';
import { NavController, IonicPage } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the CustomCoursesBlockComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-courses-block',
  templateUrl: 'custom-courses-block.html',
  styleUrls: ['/custom-courses-block.css']
})

export class CustomCoursesBlockComponent implements OnInit {
  courses = 'Compulsory Courses';
  coursesCategory: Observable<any>;
  coursesList = [];
  userId: number;
  siteurl: string;
  token: string;
  baseUrl: string;
  manual = [];
  self = [];

  constructor(public httpClient: HttpClient, public sitesProvider: CoreSitesProvider, public SpotonCustomProvider : SpotonCustomProvider, private navCtrl: NavController, public toastCtrl: ToastController) {
    this.coursesCategory = this.SpotonCustomProvider.fetchCoursesCategory( this.SpotonCustomProvider.coursesCategory );
    
    this.coursesCategory
    .subscribe((data) => {
      this.coursesList = data.data;

      this.coursesList.forEach(element => {
        if(element.enroltype == "manual") {
          this.manual.push(element);
        } else {
          this.self.push(element);
        }
      });

    }, (err) =>{
      const toast = this.toastCtrl.create({
        message: err.message,
        duration: 2000,
        position: "bottom"
      });
      toast.present(); 
    });
    
  }

  ngOnInit(): void {
    console.log("CustomCoursesBlockComponent Component");
  } 

  contentRedirect(course:any) {
    this.navCtrl.push('CoreCourseSectionPage', { course: course });
  }

  gradesRedirect(course:any) {
    this.navCtrl.push('CoreCourseSectionPage', { course: course });
  }

}
