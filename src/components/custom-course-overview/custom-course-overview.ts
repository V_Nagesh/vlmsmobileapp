import { Component } from '@angular/core';
import { CoreSitesProvider } from '@providers/sites';
import { SpotonCustomProvider } from '@providers/spoton-custom/spoton-custom';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ToastController  } from 'ionic-angular';

/**
 * Generated class for the CustomCourseOverviewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-course-overview',
  templateUrl: 'custom-course-overview.html',
  styleUrls: ['/custom-course-overview.css']
})
export class CustomCourseOverviewComponent {
  courseView: Observable<any>;
  courseOverView = {};
  userId:number;
  siteurl: string;
  token: string;
  baseUrl: string;
  completed : any;
  inProgress : any;
  notStarted : any;
  chartData: any;

  constructor(public httpClient: HttpClient, public SpotonCustomProvider : SpotonCustomProvider, public sitesProvider: CoreSitesProvider, public toastCtrl: ToastController) {
    this.courseView = this.SpotonCustomProvider.fetchCourseView( this.SpotonCustomProvider.courseView );
    this.courseView
    .subscribe( (response: any) => {
      this.chartData = response;

      if(response.code === 100){
        this.courseOverView = { 
              type: 'doughnut',
              data: [ 1, 1, 1 ],   
              labels: ['Completed', 'In-Progress', 'Not Started']
            }
      } else if(response.code === 200){
        this.completed = response.data.completed;
        this.inProgress = response.data.inprogress;
        this.notStarted = response.data.notstarted;

        this.courseOverView = { type: 'doughnut',
            data: {
              datasets: [{
                    data: [ this.completed, this.inProgress, this.notStarted ],
                    backgroundColor: ['#32CD32', '#54899F', '#d0d0d0']
                    
                  }],
              labels: ['Completed', 'In-Progress', 'Not Started']
            },
        }
      }
    }, (err) => {
      const toast = this.toastCtrl.create({
        message: err.message + ' Please try again.',
        duration: 2000,
        position: "bottom"
      });
      toast.present();
    });
    
  }

}
