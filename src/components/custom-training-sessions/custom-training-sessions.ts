import { Component } from '@angular/core';
import { ToastController, AlertController  } from 'ionic-angular';
import { CoreSitesProvider } from '@providers/sites';
import { SpotonCustomProvider } from '@providers/spoton-custom/spoton-custom';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { CoreDomUtilsProvider } from '@providers/utils/dom';

/**
 * Generated class for the CustomTrainingSessionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-training-sessions',
  templateUrl: 'custom-training-sessions.html',
  styleUrls: ['/custom-training-sessions.css']
})
export class CustomTrainingSessionsComponent {
  trainings: Observable<any>;
  trainingSession = [];
  userId: number;
  siteurl: string;
  shownGroup = null;
  signupIcon = true;
  signUpSession: Observable<any>;
  token: string;

  constructor(public httpClient: HttpClient, public SpotonCustomProvider : SpotonCustomProvider, private domUtils: CoreDomUtilsProvider, public sitesProvider: CoreSitesProvider, public toastCtrl: ToastController, public alertCtrl: AlertController) {
    this.trainingSessions();
  }

  trainingSessions(): void {
    this.trainings = this.SpotonCustomProvider.fetchTrainings( this.SpotonCustomProvider.trainings );
    this.trainings
    .subscribe( data => {
      this.trainingSession = data.data;
    },
    (err) => {
      const toast = this.toastCtrl.create({
        message: err.message + ' Please try again.',
        duration: 2000,
        position: "bottom"
      });
      toast.present();
    });
  }

  toggleGroup( group:any ) {
    if (this.isGroupShown( group )) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  }

  isGroupShown( group:any ) {
      return this.shownGroup === group;
  }

  signUp(e?: Event, session?: any, training?: any): void {
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    console.log("signUp ", e);
    const modal = this.domUtils.showModalLoading();

    this.signUpSession = this.SpotonCustomProvider.fetchTrainingSignUp( this.SpotonCustomProvider.trainingSignUp, session.id, training.id );
    this.signUpSession
    .subscribe( data => {
      modal.dismiss();

      if(data.code === 100) {
        const alert = this.alertCtrl.create({
          title: 'Please Confirm',
          subTitle: "You are already send the signup request for this session",
          buttons: ['OK']
        });
        alert.present();
      } else if (data.code === 200) {
        const alert = this.alertCtrl.create({
          title: 'Congratulations...',
          subTitle: "You sign up successfully",
          buttons: ['OK']
        });
        alert.present();

        this.trainingSessions();
      }
    },
    (err) => {
      const alert = this.alertCtrl.create({
        title: 'Please Confirm',
        subTitle: err.message,
        buttons: ['OK']
      });
      alert.present();
    });

  }

  cancel(e?: Event, session?: any): void {
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    const modal = this.domUtils.showModalLoading();

    this.signUpSession = this.SpotonCustomProvider.fetchTrainingCancel( this.SpotonCustomProvider.trainingCancel, session.id );
    this.signUpSession
    .subscribe( data => {
      modal.dismiss();

      if(data.code === 100) {
        const alert = this.alertCtrl.create({
          title: 'Please Confirm',
          subTitle: data.message,
          buttons: ['OK']
        });
        alert.present();
      } else if (data.code === 200) {
        this.trainingSessions();

        const alert = this.alertCtrl.create({
          subTitle: "You are successfully cancel your registration.",
          buttons: ['OK']
        });
        alert.present();
      }
    },
    (err) => {
      const alert = this.alertCtrl.create({
        title: 'Please Confirm',
        subTitle: err.message,
        buttons: ['OK']
      });
      alert.present();
    });
  }

  emptyClick(e?: Event): void {
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    console.log("after login emptyClick");
  }

}
