import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { catchError, tap } from 'rxjs/operators';
import { CoreSitesProvider } from '@providers/sites';

/*
  Generated class for the SpotonCustomProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SpotonCustomProvider {
  userId: number;
  baseUrl: string;
  token: string;
  coursesCategory:string;
  catalog:string;
  unenrolUrl:string;
  enrolUrl:string;
  courseView:string;
  learningPlan:string;
  trainings:string;
  trainingSignUp:string;
  trainingCancel:string;

  constructor(public http: HttpClient, public sitesProvider: CoreSitesProvider) {
    this.baseUrl = sitesProvider.getCurrentSite().getURL();
    this.userId = sitesProvider.getCurrentSite().infos.userid;
    this.token = sitesProvider.getCurrentSite().token;

    this.courseView = this.baseUrl + '/local/spotonerp/client.php?action=user_courses_overview&userid=' + this.userId + '&token=' + this.token;
    this.coursesCategory = this.baseUrl + '/local/spotonerp/client.php?action=core_enrol_get_users_courses&userid=' + this.userId + '&token=' + this.token;
    this.learningPlan = this.baseUrl + '/local/spotonerp/client.php?action=get_learning_plans&userid=' + this.userId + '&token=' + this.token;
    
    this.trainings = this.baseUrl + '/local/spotonerp/client.php?action=get_user_sessions&userid=' + this.userId + '&token=' + this.token;
    this.trainingSignUp = this.baseUrl + '/local/spotonerp/client.php?action=user_signup_sessions';
    this.trainingCancel = this.baseUrl + '/local/spotonerp/client.php?action=user_cancel_sessions';

    this.catalog = this.baseUrl + '/local/spotonerp/client.php?action=get_catalog_courses&userid=' + this.userId + '&token=' + this.token;
    this.enrolUrl = this.baseUrl + '/local/spotonerp/client.php?action=enrol_user&token=';
    this.unenrolUrl = this.baseUrl + '/local/spotonerp/client.php?action=unenrol_user&token=';
    
  }

  fetchCoursesCategory( category ): Observable<any> { 
    return this.http.get<any[]>( category ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchCourseView( courseView ): Observable<any> { 
    return this.http.get<any[]>( courseView ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchLearningPlan( learningPlan ): Observable<any> { 
    return this.http.get<any[]>( learningPlan ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchTrainings( trainings ): Observable<any> { 
    return this.http.get<any[]>( trainings ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchTrainingSignUp( trainingSignUp,  sessionId, trainingId ): Observable<any> { 
    let trainingSign = trainingSignUp + '&userid=' + this.userId + '&sessionid=' +  sessionId + '&facetofaceid=' + trainingId + '&token=' + this.token;
    return this.http.get<any[]>( trainingSign ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchTrainingCancel( trainingCancel,  sessionId ): Observable<any> { 
    let trainingCancellation = trainingCancel + '&userid=' + this.userId + '&sessionid=' +  sessionId + '&cancelreason=%27Not%20Intrested%27' + '&token=' + this.token;
    return this.http.get<any[]>( trainingCancellation ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchCatalog( catalog ): Observable<any> { 
    return this.http.get<any[]>( catalog ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchEnrollStatus( enroll, courseId, enrollId ): Observable<any> { 
    let courseEnroll = enroll + this.token + '&userid=' + this.userId + '&courseid=' + courseId + '&instanceid=' + enrollId;
    return this.http.get<any[]>( courseEnroll ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }

  fetchUnenrollStatus( unenroll, courseId, enrollId ): Observable<any> { 
    let courseUnenroll = unenroll + this.token + '&userid=' + this.userId + '&courseid=' + courseId + '&instanceid=' + enrollId;
    return this.http.get<any[]>( courseUnenroll ).pipe(
            tap( data => console.log( 'server data:', data ) ) );
  }
}
