import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { CoreSitesProvider } from '@providers/sites';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the MyLearningPlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-learning-plan',
  templateUrl: 'my-learning-plan.html',
})
export class MyLearningPlanPage {
  learningData: Observable<any>;
  learningPlanData = [];
  plans: any;
  planId: number;
  siteurl: string;
  token: string;
  baseUrl: string;
  learningPlanName: string;
  loadingstatus = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public httpClient: HttpClient, public sitesProvider: CoreSitesProvider, public toastCtrl: ToastController) {
    this.plans = navParams.get('plans');
    this.learningPlanName = this.plans.planname;
    this.planId = this.plans.id; //26
    this.siteurl = sitesProvider.getCurrentSite().getURL();
    this.token = sitesProvider.getCurrentSite().getToken();
    this.baseUrl = '/local/spotonerp/client.php?action=get_plan_courses&planid=';

    this.learningData = this.httpClient.get( this.siteurl + this.baseUrl + this.planId + '&token=' + this.token );
    this.learningData
    .subscribe( (data) => {
      this.loadingstatus = true;

      if(this.loadingstatus) {
        this.learningPlanData = data.data;
      }
    },
    (err) => {
      const toast = this.toastCtrl.create({
        message: err.message + ' Please try again.',
        duration: 2000,
        position: "bottom"
      });
      toast.present();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyLearningPlanPage');
  }

  myCoursesPage(course) {
    this.navCtrl.push('CoreCourseSectionPage', { course: course });
  }

}
