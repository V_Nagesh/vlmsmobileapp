import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyLearningPlanPage } from './my-learning-plan';

@NgModule({
  declarations: [
    MyLearningPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(MyLearningPlanPage),
  ],
})
export class MyLearningPlanPageModule {}
