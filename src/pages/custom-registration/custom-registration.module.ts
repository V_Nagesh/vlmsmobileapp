import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomRegistrationPage } from './custom-registration';
import { CoreDirectivesModule } from '@directives/directives.module';
import { CoreComponentsModule } from '@components/components.module';
import { CoreUserComponentsModule } from "../../core/user/components/components.module";

@NgModule({
  declarations: [
    CustomRegistrationPage,
  ],
  imports: [
    CoreComponentsModule,
    CoreDirectivesModule,
    CoreUserComponentsModule,
    IonicPageModule.forChild(CustomRegistrationPage),
  ],
})
export class CustomRegistrationPageModule {}
