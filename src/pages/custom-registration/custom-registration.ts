import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { CoreDomUtilsProvider } from '@providers/utils/dom';

/**
 * Generated class for the CustomRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-custom-registration',
  templateUrl: 'custom-registration.html',
})
export class CustomRegistrationPage {
  registrationForm: FormGroup;
  siteurl: string;
  baseUrl: string;
  registerUser: Observable<any>;
  pageLoaded = true;

  constructor(public navCtrl: NavController, private domUtils: CoreDomUtilsProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public fb: FormBuilder, public alertCtrl: AlertController, public toastCtrl: ToastController, public httpClient: HttpClient) {
    this.siteurl = this.navParams.data;
    this.baseUrl = '/local/spotonerp/client.php?action=signup_user';

    this.registrationForm = fb.group({
        phone: ['', Validators.compose([Validators.required, Validators.minLength(9), Validators.maxLength(10)])],
        firstName: ['', Validators.required],
        surName: ['', Validators.required],
        email: ['', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomRegistrationPage ', this.siteurl);
  }

  register(e?: Event): void {
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    const phone = this.registrationForm.value.phone,
          firstName = this.registrationForm.value.firstName,
          surName = this.registrationForm.value.surName,
          email = this.registrationForm.value.email;

    const modal = this.domUtils.showModalLoading();

    this.registerUser = this.httpClient.get( this.siteurl + this.baseUrl + '&phone=' + phone + '&firstname=' + firstName + '&lastname=' + surName + '&email=' + email);
    this.registerUser.subscribe((data: any) => {
      modal.dismiss();

      if(data.code === 100) {
        const alert = this.alertCtrl.create({
          title: 'Please Confirm',
          subTitle: data.message,
          buttons: ['OK']
        });
        alert.present();
      } else if (data.code === 200) {
        const alert = this.alertCtrl.create({
          title: 'Welcome...',
          subTitle: data.data.message,
          buttons: ['OK']
        });
        alert.present();
        
        setTimeout(() => {
          this.navCtrl.push('CoreLoginCredentialsPage'); 
        }, 2000); 

        this.registrationForm.controls['phone'].reset();
        this.registrationForm.controls['firstName'].reset();
        this.registrationForm.controls['surName'].reset();
        this.registrationForm.controls['email'].reset();
      }

    }, (err) =>{
      const alert = this.alertCtrl.create({
        title: 'Please Confirm',
        subTitle: err.message,
        buttons: ['OK']
      });
      alert.present(); 
    });
  }

}
